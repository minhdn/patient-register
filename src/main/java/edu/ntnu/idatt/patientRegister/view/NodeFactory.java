package edu.ntnu.idatt.patientRegister.view;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class NodeFactory {
    public static Node getNode(String nodeType) {
        if (nodeType == null) {
            return null;
        }
        if (nodeType.equalsIgnoreCase("MENUBAR")) {
            return new MenuBar();
        } else if (nodeType.equalsIgnoreCase("TOOLBAR")) {
            return new ToolBar();
        } else if (nodeType.equalsIgnoreCase("BORDERPANE")) {
            return new BorderPane();
        } else if(nodeType.equalsIgnoreCase("VBox")) {
            return new VBox();
        } else if(nodeType.equalsIgnoreCase("TABLEVIEW")) {
            return new TableView<>();
        } else if(nodeType.equalsIgnoreCase("BUTTON")) {
            return new Button();
        } else if(nodeType.equalsIgnoreCase("TEXTFIELD")) {
            return new TextField();
        } else if(nodeType.equalsIgnoreCase("LABEL")) {
            return new Label();
        }
         return null;
    }
}
