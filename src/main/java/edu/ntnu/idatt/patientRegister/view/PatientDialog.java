package edu.ntnu.idatt.patientRegister.view;

import edu.ntnu.idatt.patientRegister.Patient;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

public class PatientDialog extends Dialog<Patient>{
    public boolean editPatient = false;
    public Patient editablePatient;

    /**
     * Constructor for the patient dialog.
     *
     * @param patient The patient that will be edited, if that is the choice
     */
    public PatientDialog(Patient patient){
        this.editPatient = true;
        this.editablePatient = patient;

        editPatient();
    }

    public PatientDialog(){
        editPatient();
    }

    /**
     * Edit or add a patient.
     */
    public void editPatient() {
        setTitle("Patient");
        ButtonType cancelButton = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, cancelButton);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);

        TextField firstname = new TextField();
        TextField lastname = new TextField();
        TextField SSN = new TextField();
        TextField generalPractitioner = new TextField();
        TextField diagnosis = new TextField();

        grid.add(new Label("First name:"), 0, 0);
        grid.add(firstname, 1, 0);
        grid.add(new Label("Last name:"), 0, 1);
        grid.add(lastname, 1, 1);
        grid.add(new Label("Social Security Number:"), 0,2);
        grid.add(SSN, 1, 2);
        grid.add(new Label("General Practitioner:"), 0,3);
        grid.add(generalPractitioner, 1, 3);
        grid.add(new Label("Diagnosis:"), 0,4);
        grid.add(diagnosis, 1, 4);

        // Existing data will be shown in the textfields if patient is being edited.
        if(editPatient){
            firstname.setText(editablePatient.getFirstname());
            lastname.setText(editablePatient.getLastname());
            SSN.setText(editablePatient.getSSN());
            generalPractitioner.setText(editablePatient.getGeneralPractitioner());
            diagnosis.setText(editablePatient.getDiagnosis());
        }
        getDialogPane().setContent(grid);

        setResultConverter((ButtonType button) -> {
            if(button == cancelButton){
                return null;
            }else {
                return new Patient(firstname.getText(), lastname.getText(), SSN.getText(), generalPractitioner.getText(), diagnosis.getText());
            }
        });
    }
}
