package edu.ntnu.idatt.patientRegister;

public class Patient {
    private String firstname;
    private String lastname;
    private String SSN;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * The constructor for a simple patient class.
     * @param firstname First name
     * @param lastname Last name
     * @param SSN Social security number
     * @param diagnosis Diagnosis
     * @param generalPractitioner General practitioner
     */
    public Patient(String firstname, String lastname, String SSN, String diagnosis, String generalPractitioner) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.SSN = SSN;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Getters and setters
     */
    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getSSN() {
        return SSN;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setSSN(String SSN) {
        this.SSN = SSN;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }
}
