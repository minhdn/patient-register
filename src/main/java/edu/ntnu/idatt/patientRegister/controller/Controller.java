package edu.ntnu.idatt.patientRegister.controller;

import edu.ntnu.idatt.patientRegister.Patient;
import edu.ntnu.idatt.patientRegister.model.PatientRegister;
import edu.ntnu.idatt.patientRegister.view.PatientDialog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

import java.io.*;
import java.util.ArrayList;
import java.util.Optional;

public class Controller {
    PatientRegister patientRegister;
    private ObservableList<Patient> observablePatientList;

    @FXML
    public TableView<Patient> patientTableView;
    @FXML
    public TableColumn<Patient, String> firstNameColumn;
    @FXML
    public TableColumn<Patient, String> lastNameColumn;
    @FXML
    public TableColumn<Patient, String> SSNColumn;
    @FXML
    public VBox parent;

    /**
     * The application will show information about patients when it is initialized.
     */
    public void initialize(){
        this.patientRegister = new PatientRegister();
        this.observablePatientList = FXCollections.observableArrayList(patientRegister.getPatients());

        this.firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstname"));
        this.lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastname"));
        this.SSNColumn.setCellValueFactory(new PropertyValueFactory<>("SSN"));

        this.patientTableView.setItems(this.observablePatientList);
    }

    /**
     * Dialog button that show information about the application.
     */
    public void about(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText("Patient Register");
        alert.setContentText("An application made by me.\nPlease do not contact me if there is something wrong with it.");

        alert.showAndWait();
    }

    /**
     * Add a new patient. The dialog will stay open until the user clicks cancel even if there are exceptions.
     */
    public void addPatient(){
        PatientDialog patientDialog = new PatientDialog();
        boolean showDialog = true;
        while(showDialog){
            Optional<Patient> result = patientDialog.showAndWait();
            if (result.isPresent()) {
                try{
                    Patient newPatient = result.get();
                    patientRegister.addNewPatient(newPatient.getFirstname(), newPatient.getLastname(), newPatient.getSSN(), newPatient.getGeneralPractitioner(), newPatient.getDiagnosis());
                    updateObservableList();
                    showDialog = false;
                } catch (IllegalArgumentException exception){
                    invalidInput(exception.getMessage());
                }
            } else {
                showDialog = false;
            }
        }
    }

    /**
     * Edit an existing patient. The user has to select a patient and the edit dialog will stay open until the user clicks cancel.
     */
    public void editPatient() {
        boolean showDialog = true;
        if(patientTableView.getSelectionModel().getSelectedItem() == null) {
            invalidSelection();
        } else {
            Patient existingPatient = patientTableView.getSelectionModel().getSelectedItem();
            PatientDialog patientDialog = new PatientDialog(existingPatient);
            while(showDialog){
                Optional<Patient> result = patientDialog.showAndWait();
                if (result.isPresent()) {
                    try{
                        Patient editPatient = result.get();
                        patientRegister.editPatient(existingPatient, editPatient.getFirstname(), editPatient.getLastname(), editPatient.getSSN(), editPatient.getGeneralPractitioner(), editPatient.getDiagnosis());
                        updateObservableList();
                        showDialog = false;
                    } catch (IllegalArgumentException exception){
                        invalidInput(exception.getMessage());
                    }
                } else {
                    showDialog = false;
                }
            }
        }
    }

    /**
     * Deletes an existing patient. The user has to select a patient to delete.
     */
    public void deletePatient(){
        if(patientTableView.getSelectionModel().getSelectedItem() == null) {
            invalidSelection();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Delete patient");
            alert.setHeaderText("Delete confirmation");
            alert.setContentText("Are you sure you want to delete this item?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent()) {
                patientRegister.deletePatient(patientTableView.getSelectionModel().getSelectedItem());
            }
            updateObservableList();
        }
    }

    /**
     * Updates the list so the tableview is always up to date.
     */
    public void updateObservableList(){
        this.observablePatientList.setAll(this.patientRegister.getPatients());
    }

    /**
     * Exits the application
     */
    public void exit(){
        System.exit(0);
    }

    /**
     * Imports a CSV file. The application will read from the csv file, line by line, and create a new patient
     * for each line. Every invalid line or inputs will be collected and shown to the user in the end.
     */
    public void importFile() {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(parent.getScene().getWindow());

        String extension = "";
        int i = file.getName().lastIndexOf('.');
        if (i >= 0) { extension = file.getName().substring(i+1); }

        if(extension.equals("csv")){
            ArrayList<String> invalidPatients = new ArrayList<>();
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    String[] patientInformation = line.split(";");
                    try{
                        patientRegister.addNewPatient(patientInformation[0], patientInformation[1], patientInformation[3], patientInformation[2], "");
                    } catch(IllegalArgumentException exception){
                        invalidPatients.add(line);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(!invalidPatients.isEmpty()){
                // Shows all invalid patients after importing
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("There was an error adding following patients");
                TextArea textArea = new TextArea();
                String allPatients = "";
                for(String string : invalidPatients){
                    allPatients += (string) + "\n";
                }
                textArea.setText(allPatients);
                alert.getDialogPane().setContent(textArea);
                alert.showAndWait();
            }
            updateObservableList();
        } else {
            fileError();
        }
    }

    /**
     * Exports a CSV file. The user has to choose a directory and name for the file. If a file with the same
     * name already exists, the user will have to choose to either rename the file or overwrite the old file.
     */
    public void exportFile() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(parent.getScene().getWindow());

        boolean showDialog = true;
        if(selectedDirectory.isDirectory()){
            while(showDialog){
                TextInputDialog dialog = new TextInputDialog("Patients");
                dialog.setTitle("Export file");
                dialog.setHeaderText("Name your file");
                dialog.setContentText("File name: ");

                Optional<String> result = dialog.showAndWait();

                if (result.isPresent()){
                    try {
                        File newFile = new File(selectedDirectory.getAbsolutePath() + "/" + result.get() + ".csv");
                        if(newFile.createNewFile()){
                            FileOutputStream fos = new FileOutputStream(newFile);
                            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fos));

                            for (Patient patient : patientRegister.getPatients()) {
                                bufferedWriter.write(patient.getFirstname() + ";" + patient.getLastname() + ";" + patient.getGeneralPractitioner() + ";" + patient.getSSN());
                                bufferedWriter.newLine();
                            }
                            bufferedWriter.close();
                            showDialog = false;
                        }else {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("Error");
                            alert.setContentText("A file with same name exists. Do you want to overwrite it?");
                            ButtonType yesButton = new ButtonType("Yes", ButtonBar.ButtonData.YES);
                            ButtonType noButton = new ButtonType("No", ButtonBar.ButtonData.NO);
                            alert.getButtonTypes().setAll(yesButton, noButton);
                            Optional<ButtonType> buttonResult = alert.showAndWait();

                            if(buttonResult.get().equals(yesButton)){
                                FileOutputStream fos = new FileOutputStream(newFile, false);
                                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fos));

                                for (Patient patient : patientRegister.getPatients()) {
                                    bufferedWriter.write(patient.getFirstname() + ";" + patient.getLastname() + ";" + patient.getGeneralPractitioner() + ";" + patient.getSSN());
                                    bufferedWriter.newLine();
                                }
                                bufferedWriter.close();
                                showDialog = false;
                            }
                        }
                    } catch (IOException exception){
                        exception.printStackTrace();
                    }
                } else {
                    showDialog = false;
                }
            }
        } else {
            unknownError();
        }
    }

    /**
     * An error will be shown if there is any invalid inputs when adding or editing a patient,
     * for example an empty name.
     *
     * @param exception the error/exception
     */
    public void invalidInput (String exception){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setContentText(exception);
        alert.showAndWait();
    }

    /**
     * An error will be shown if the user does not select a patient when editing or deleting a patient.
     */
    public void invalidSelection (){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setContentText("Please select a patient");
        alert.showAndWait();
    }

    /**
     * An error will be shown if the user does not choose a CSV file when importing a file.
     */
    public void fileError(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setContentText("The chosen file type is not valid.\n" +
                "Please choose another file by clicking on Select File or cancel the operation");
        ButtonType selectButton = new ButtonType("Select File");
        ButtonType cancelButton = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(selectButton, cancelButton);
        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == selectButton){
            importFile();
        }
    }

    /**
     * An error dialog for unknown errors.
     */
    public void unknownError(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setContentText("An unknown error has occured. Please try again.");
        alert.showAndWait();
    }
}
