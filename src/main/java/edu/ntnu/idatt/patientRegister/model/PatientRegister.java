package edu.ntnu.idatt.patientRegister.model;

import edu.ntnu.idatt.patientRegister.Patient;

import java.util.ArrayList;
import java.util.List;

public class PatientRegister {
    private ArrayList<Patient> patientRegister = new ArrayList<>();

    /**
     * Method for adding a new patient to the patient register. It is not required to fill out
     * general practitioner or diagnosis and the method will therefore not check for exceptions here.
     *
     * This method does not check for special characters as there are many unique names in the world.
     *
     * @param firstname First name
     * @param lastname Last name
     * @param SSN Social Security Number, must be 11 numbers
     * @param generalPractitioner General Practitioner
     * @param diagnosis Diagnosis
     */
    public void addNewPatient(String firstname, String lastname, String SSN, String generalPractitioner, String diagnosis) {
        if(firstname == null || lastname == null || SSN == null || firstname.length() == 0 || lastname.length() == 0 || !(SSN.length() == 11)){
            throw new IllegalArgumentException("Information is invalid");
        } else {
            if(patientRegister.stream().anyMatch(patient -> patient.getSSN().equals(SSN))){
                throw new IllegalArgumentException("This patient already exists.");
            }else {
                Patient newPatient = new Patient(firstname, lastname, SSN, generalPractitioner, diagnosis);
                patientRegister.add(newPatient);
            }
        }
    }

    public List<Patient> getPatients(){
        return patientRegister;
    }

    /**
     * Edit an existing patient to update information regarding this patient.
     *
     * @param editPatient The patient that will be edited
     * @param firstname New first name
     * @param lastname New last name
     * @param SSN New Social Security Number
     */
    public void editPatient(Patient editPatient, String firstname, String lastname, String SSN, String generalPractitioner, String diagnosis) {
        if(firstname == null || lastname == null || SSN == null || firstname.length() == 0 || lastname.length() == 0 || !(SSN.length() == 11)){
            throw new IllegalArgumentException("Information is invalid");
        } else {
            for (Patient patient : patientRegister) {
                if (editPatient.equals(patient)) {
                    patient.setFirstname(firstname);
                    patient.setLastname(lastname);
                    patient.setSSN(SSN);
                    patient.setGeneralPractitioner(generalPractitioner);
                    patient.setDiagnosis(diagnosis);
                }
            }
        }
    }

    /**
     * Delete a patient from the register.
     *
     * @param deletePatient Patient to be deleted
     */
    public void deletePatient(Patient deletePatient) {
        patientRegister.removeIf(deletePatient::equals);
    }
}
