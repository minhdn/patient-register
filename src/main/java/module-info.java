module PatientRegister {
    requires javafx.controls;
    requires javafx.fxml;

    opens edu.ntnu.idatt.patientRegister.view to javafx.fxml;
    opens edu.ntnu.idatt.patientRegister.model to javafx.fxml;
    opens edu.ntnu.idatt.patientRegister to javafx.fxml;
    exports edu.ntnu.idatt.patientRegister;
    exports edu.ntnu.idatt.patientRegister.controller;
}