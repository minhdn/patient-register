package edu.ntnu.idatt.patientRegister.model;

import edu.ntnu.idatt.patientRegister.Patient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class PatientRegisterTest {
    PatientRegister patientRegister = new PatientRegister();

    @Nested
     public class AddNewPatient{

        @Test
        void EmptyFirstName(){
            Assertions.assertThrows(IllegalArgumentException.class, ()->
                    patientRegister.addNewPatient("", "LastName", "12345678910", "", ""));
        }
        @Test
        void EmptyLastName(){
            Assertions.assertThrows(IllegalArgumentException.class, ()->
                    patientRegister.addNewPatient("Firstname", "", "12345678910", "", ""));
        }
        @Test
        void UnderElevenNumbersForSSN(){
            Assertions.assertThrows(IllegalArgumentException.class, ()->
                    patientRegister.addNewPatient("Firstname", "LastName", "123", "", ""));
        }
        @Test
        void OverElevenNumbersForSSN(){
            Assertions.assertThrows(IllegalArgumentException.class, ()->
                    patientRegister.addNewPatient("Firstname", "LastName", "1233414342342342", "", ""));
        }
        @Test
        void SuccessfullyAddPatient(){
            patientRegister.addNewPatient("first", "last", "12345678910", "", "");
            Assertions.assertEquals(1, patientRegister.getPatients().size());
        }
    }

    @Nested
    public class EditPatient {

        @BeforeEach
        void initAddPatient() {
            patientRegister.addNewPatient("Firstname", "Lastname", "12345678910", "", "");
        }

        @Test
        void InvalidName(){
            Patient patient = patientRegister.getPatients().get(0);
            Assertions.assertThrows(IllegalArgumentException.class, () ->
                    patientRegister.editPatient(patient, "", "", "12345678910", "", ""));
        }
        @Test
        void InvalidSSN(){
            Patient patient = patientRegister.getPatients().get(0);
            Assertions.assertThrows(IllegalArgumentException.class, () ->
                    patientRegister.editPatient(patient, "firstname", "lastname", "1", "", ""));
        }
    }
}