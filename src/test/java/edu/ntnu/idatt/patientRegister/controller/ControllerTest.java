package edu.ntnu.idatt.patientRegister.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.*;

class ControllerTest {
    @Nested
    public class ReadAndWriteToFile{

        @Test
        void ReadFromFile() throws IOException {
            String expected_value = "firstname;lastname;socialSecurityNumber;generalPractitioner;diagnosis";
            String file ="src/test/java/edu/ntnu/idatt/patientRegister/controller/readFileTest";

            BufferedReader reader = new BufferedReader(new FileReader(file));
            String currentLine = reader.readLine();
            reader.close();

            Assertions.assertEquals(expected_value, currentLine);
        }

        @Test
        void WriteToFile() throws IOException {
            String expected_value = "firstname;lastname;socialSecurityNumber;generalPractitioner;diagnosis";
            String file ="src/test/java/edu/ntnu/idatt/patientRegister/controller/writeFileTest";

            FileOutputStream fos = new FileOutputStream(file);
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fos));
            bufferedWriter.write("firstname;lastname;socialSecurityNumber;generalPractitioner;diagnosis");
            bufferedWriter.close();

            BufferedReader reader = new BufferedReader(new FileReader(file));
            String currentLine = reader.readLine();
            reader.close();

            Assertions.assertEquals(expected_value, currentLine);
        }
    }
}